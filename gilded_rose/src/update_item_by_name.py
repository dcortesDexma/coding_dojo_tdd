__author__ = 'dcortes'

from gilded_rose.src.abstract_update import AbstractUpdate

class UpdateItemByName(AbstractUpdate):

    special_objects = {"Sulfuras": "sulfuras",
                       "Aged Brie": "aged_brie",
                       "Backstage Passes": "backstage_passes"}

    def __init__(self):
        AbstractUpdate.__init__(self)

    def _basic_update(self, item, quality):
        item.sell_in -= 1
        item.quality += quality if item.sell_in >= 0 else quality * 2

    def _update_quality_aged_brie(self, item, factor):
        quality = 1 * factor
        self._basic_update(item, quality)
        if item.quality > 50:
            item.quality = 50

    def _update_quality_backstage_passes(self, item, factor):
        quality = 3 if item.sell_in <= 5 else 2 if item.sell_in <= 10 else 1
        quality *= factor
        self._basic_update(item, quality)
        if item.quality > 50:
            item.quality = 50

    def _update_quality_sulfuras(self, item, factor):
        pass

    def _update_quality_basic(self, item, factor):
        quality = -1 * factor
        self._basic_update(item, quality)
        if item.quality < 0:
            item.quality = 0

    def update_quality(self, item, factor):
        methodToCall = getattr(self, '_update_quality_%s' % self.mapper_values[id(item)])
        methodToCall(item, factor)
