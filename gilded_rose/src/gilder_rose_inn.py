__author__ = 'dcortes'


class GilderRoseInn(object):

    def __init__(self, UpdateItemByName, UpdateItemByType, items):
        self.items = items
        self.update_by_name = UpdateItemByName
        self.update_by_type = UpdateItemByType
        [self.update_by_type.map_type(item) for item in items]
        [self.update_by_name.map_type(item) for item in items]

    def update_quality(self):
        for item in self.items:
            factor = self.update_by_type.update_quality(item)
            self.update_by_name.update_quality(item, factor)
