__author__ = 'dcortes'
import abc


class AbstractUpdate():

    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.mapper_values = {}

    def _get_type(self, item):
        for special_object in self.special_objects.keys():
            if special_object in item.name:
                return self.special_objects[special_object]
        return "basic"

    def map_type(self, item):
        self.mapper_values[id(item)] = self._get_type(item)

    @abc.abstractmethod
    def update_quality(self, *kargs):
        pass
