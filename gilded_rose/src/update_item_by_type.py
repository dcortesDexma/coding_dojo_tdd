__author__ = 'dcortes'

from gilded_rose.src.abstract_update import AbstractUpdate


class UpdateItemByType(AbstractUpdate):

    special_objects = {"Conjured": "conjured"}

    def __init__(self):
        AbstractUpdate.__init__(self)

    def _update_quality_conjured(self):
        return 2

    def _update_quality_basic(self):
        return 1

    def update_quality(self, item):
        methodToCall = getattr(self, '_update_quality_%s' % self.mapper_values[id(item)])
        return methodToCall()
