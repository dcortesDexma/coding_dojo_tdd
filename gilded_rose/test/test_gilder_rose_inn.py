__author__ = 'dcortes'

from gilded_rose.src.gilder_rose_inn import GilderRoseInn
from gilded_rose.src.update_item_by_name import UpdateItemByName
from gilded_rose.src.update_item_by_type import UpdateItemByType
from gilded_rose.src.item import Item


class TestRepositoryAnalysis():

    _collection = "test_analysis"

    def setup_method(self, method):
        self.update_item_by_name = UpdateItemByName()
        self.update_item_by_type = UpdateItemByType()

    def test_not_items(self):
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [])
        gilderRose.update_quality()
        assert(len(gilderRose.items) == 0)

    def test_basic_item(self):
        item_test = Item("prueba Miam miam", 20, 50)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 49)
        assert(gilderRose.items[0].sell_in == 19)

    def test_passed_item(self):
        item_test = Item("prueba Miam miam", 0, 50)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 48)
        assert(gilderRose.items[0].sell_in == -1)

    def test_basic_item_not_less_zero(self):
        item_test = Item("prueba Miam miam", 20, 0)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 0)
        assert(gilderRose.items[0].sell_in == 19)

    def test_item_aged_brie_gilder(self):
        item_test = Item("Aged Brie", 20, 49)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 50)
        assert(gilderRose.items[0].sell_in == 19)

    def test_item_aged_brie_gilder_not_bigger_fifthy(self):
        item_test = Item("Aged Brie", 20, 50)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 50)
        assert(gilderRose.items[0].sell_in == 19)

    def test_item_sulfuras(self):
        item_test = Item("Sulfuras", 20, 80)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 80)
        assert(gilderRose.items[0].sell_in == 20)

    def test_item_conjured_sulfuras(self):
        item_test = Item("Conjured Sulfuras", 20, 80)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 80)
        assert(gilderRose.items[0].sell_in == 20)

    def test_item_conjured_basic(self):
        item_test = Item("Conjured basic", 20, 50)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 48)
        assert(gilderRose.items[0].sell_in == 19)

    def test_item_conjured_aged_brie(self):
        item_test = Item("Conjured Aged Brie", 20, 48)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 50)
        assert(gilderRose.items[0].sell_in == 19)

    def test_item_conjured_aged_brie_passed(self):
        item_test = Item("Conjured Aged Brie", -1, 46)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 50)
        assert(gilderRose.items[0].sell_in == -2)

    def test_item_backstage_passes(self):
        item_test = Item("Backstage Passes", 12, 46)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 47)
        assert(gilderRose.items[0].sell_in == 11)

    def test_item_backstage_passes_ten_days(self):
        item_test = Item("Backstage Passes", 10, 46)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 48)
        assert(gilderRose.items[0].sell_in == 9)

    def test_item_backstage_passes_five_days(self):
        item_test = Item("Backstage Passes", 5, 46)
        gilderRose = GilderRoseInn(self.update_item_by_name, self.update_item_by_type, [item_test])
        gilderRose.update_quality()
        assert(gilderRose.items[0].quality == 49)
        assert(gilderRose.items[0].sell_in == 4)